# Java Program #5 Quiz

**Student:** Liam Hockley

**Submitted Date:** Apr 9, 2012 10:53 pm

**Grade:** 35.0 (max 35.0)

**Instructions**
> Write a Java application (you can use one file for this one) that contains an array of 10 multiple-choice quiz questions related to your favorite hobby.  Each question contains three answer choices.  
> Also create an array that holds the correct answer to each question - A, B, and C.
> Display each question and verify that the user enters only A, B, or C as the answer -- if not, keep prompting the user until a valid response is entered.  If the user responds to a question correctly, display "Correct"; otherwise, display "The correct answer is: " and the letter of the correct answer.
> After the user answers all the questions, display the number of correct and incorrect answers.